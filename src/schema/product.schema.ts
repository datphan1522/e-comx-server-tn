import mongoose, { Schema } from "mongoose";
import { IProductModel, StatusEnum } from "../types/product.types";

const productSchema: Schema = new Schema<IProductModel>(
  {
    name: {
      type: String,
      required: true,
    },
    code: {
      type: String,
      required: true,
      unique: true,
    },
    images: [
      {
        url: { type: String },
        public_id: {
          type: String,
        },
      },
    ],
    price: {
      type: Number,
      required: true,
    },
    available: {
      type: Number,
      // default: 1,
      required: true,
    },
    description: {
      type: String,
    },
    tags: {
      type: Array,
      lowercase: true,
    },
    brand: {
      type: String,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
    },
    status: {
      type: String,
      enum: StatusEnum,
      default: StatusEnum.ACTIVE,
      lowercase: true,
    },
    color:[
      {
        type: String,
      }
    ],
    discount:{
      type: Number
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model<IProductModel>("Product", productSchema);
