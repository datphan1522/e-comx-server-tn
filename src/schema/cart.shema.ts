import mongoose, { Schema } from "mongoose";
import { ICartModel } from "../types/cart.type";


const cartSchema : Schema = new Schema<ICartModel>(
    {
        products: [
            {
              product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product",
              },
              quantity: {
                type: Number,
                default: 1,
              }
            },
          ],
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
        }
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model<ICartModel>("Cart", cartSchema);