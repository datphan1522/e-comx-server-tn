import mongoose, { Schema } from "mongoose";
import { ICouponModel } from "../types/coupon.types";
import { StatusEnum } from "../types/product.types";

const couponSchema: Schema = new Schema<ICouponModel>(
  {
    code: {
      type: String,
      lowercase: true,
      required: true,
      unique: true,
    },
    discount: {
      type: Number,
      required: true,
    },
    startDate: {
      type: Date,
      default: Date.now,
    },
    endDate: {
      type: Date,
    },
    available: {
      type: Number,
      default: 1,
    },
    status: {
      type: String,
      enum: StatusEnum,
      default: StatusEnum.ACTIVE,
      lowercase: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model<ICouponModel>("Coupon", couponSchema);
