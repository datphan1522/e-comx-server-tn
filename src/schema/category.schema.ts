import mongoose, { Schema } from "mongoose";
import { ICategoryModel } from "../types/category.types";

const categorySchema: Schema = new Schema<ICategoryModel>(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    description:{
      type: String,
    },
    image: 
      {
        url: { type: String },
        public_id: {
          type: String,
        },
      },
});

module.exports = mongoose.model<ICategoryModel>("Category", categorySchema);
