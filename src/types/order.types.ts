import mongoose, { Document } from 'mongoose';
import { IProduct } from './product.types';
import { ICoupon } from './coupon.types';

export interface IOrder {
  code: string;
  discount: number;
  shippingOrderCode?: string;
  serviceTypeId: number;
  warehouseUser: mongoose.Schema.Types.ObjectId;
  products: [
    {
      product: IProduct,
      quantity: number,
      currentPrice: number,
    },
  ];
  coupon ?: ICoupon;
  user: mongoose.Schema.Types.ObjectId;
  status: string;
  available: number;
  shippingDetail: {
    fullname: string,
    address: {
      province: {
        provinceId: number,
        provinceName: string,
      },
      district: {
        districtId: number,
        districtName: string,
      },
      ward: {
        wardId: string,
        wardName: string,
      },
      detail: string,
    },
    phone: string,
    email: string,
  };
  paymentStatus?: string;
  paymentType: string;
  totalPrice?: number;
  payDate?: string;
}

export interface IOrderModel extends Document, IOrder {}
