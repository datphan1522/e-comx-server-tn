import mongoose, { Document } from 'mongoose';
import { StatusEnum } from './product.types';

export enum RoleEnum {
  USER= "user",
  WAREHOUSE= "warehouse",
  ADMIN= "admin"
}

export interface IUser {
  username: string;
  password: string;
  email: string;
  phone?: number;
  name?: string;
  role: RoleEnum;
  status: StatusEnum;
  gender?: string;
  avatar?: string;
  address ?: {
    province: {
      provinceId: number,
      provinceName: String,
    },
    district: {
      districtId: number,
      districtName: String,
    },
    ward: {
      wardId: number,
      wardName: String,
    },
    detail: String,
  };
  signup?: string;
}

export interface IUserModel extends Document, IUser {}
