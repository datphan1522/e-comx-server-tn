import mongoose, { Document } from 'mongoose';

export enum StatusEnum {
  ACTIVE= "active",
  INACTIVE= "inactive"}

export interface IProduct {
  name: string;
  code: string;
  images: [
    {
      url: string,
      public_id: string,
    },
  ];
  price: number;
  available: number;
  description?: string;
  tags ?: string;
  brand ?: string;
  color ?: string[];
  status: StatusEnum;
  discount?: number;
  category: mongoose.Schema.Types.ObjectId;
}

export interface IProductModel extends Document, IProduct {}
