import mongoose from "mongoose"


export interface ICart {
  products?: [
    {
      product: mongoose.Schema.Types.ObjectId,
      quantity: number,
    },
  ],
  user: mongoose.Schema.Types.ObjectId
}

export interface ICartModel extends Document, ICart { }
