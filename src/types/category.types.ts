import mongoose, { Document } from 'mongoose';

export interface ICategory {
  name: string;
  image: 
    {
      url: string,
      public_id: string,
    };
  description?: String;
}

export interface ICategoryModel extends Document, ICategory {}
