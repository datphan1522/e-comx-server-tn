import mongoose, { Document } from 'mongoose';
import { StatusEnum } from './product.types';

export interface ICoupon {
  code: string;
  discount: number;
  startDate: Date;
  endDate: Date;
  available: number;
  status: StatusEnum;
}

export interface ICouponModel extends Document, ICoupon {}
